(function(w){
    var document = w.document,
        PxaFilters = w.PxaFilters || {};

    PxaFilters.filters = PxaFilters.filters || [];
    PxaFilters.filters[0] = {selectorId: '#file-categories input[type="checkbox"]', logicalOperator: 'OR', data: 'file-categories'};

    w.PxaFilters = PxaFilters;
}(window));

$(document).ready(function () {
    $('.filter-item').pxaFiltering({
        effect                          : 'slide',
        activeClassFilterItem           : 'active',
        putActiveFilterClassOnParent    : false,
        activeClassItem                 : 'active',
        logicalOperatorFilters          : 'AND',
        animationSpeed                  : 'fast',
        triggerFilterOnEvent            : 'change',
        filterAttributeName             : 'value',
        useDataToGetFilterAttribute     : false,

        onFilteringDone: function (elem) {
            alert('filtering is done');
        },

        filters: [
            {
                selectorId: '.column-filter-1 input[type="checkbox"]',
                logicalOperator: 'OR',
                data: 'categories'
            },

            {
                selectorId: '.column-filter-2 input[type="checkbox"]',
                logicalOperator: 'OR',
                data: 'categories'
            }
        ]
    });
});